suppressMessages(library(optparse))
suppressMessages(library(Biostrings))
suppressMessages(library(Rsamtools))
suppressMessages(library(dplyr))

option_list <- list(
    make_option(c("-o", "--output"), type="character", default=".",
                help="Name of file where results should be output."),
    make_option(c("--sPARTAoutput"), type="character",
                help="The path to the sPARTA output directory whose results should be analyzed."),
    make_option(c("--gff"), type="character",
                help="The gff file describing the target transcriptome. Required."),
    make_option(c("-f", "--fasta"), type="character",
                help="The fasta file describing the target genome. Required."),                
    make_option(c("-t", "--threshold"), type="integer", default=50,
                help="The minimum number of PARE reads for signal recognition."),
    make_option(c("-w", "--window"), type="integer", default=0,                 
                help="The number of nucleotides on each side of the potential signal which are ignored when calculating the best factor."),
    make_option(c("-s", "--scalefactor"), type="double", default=2,
                help="The detected signal must be >= the next strongest signal * this factor."),
    make_option(c("-m", "--minnsignal"), type="integer", default=20,
                help="When comparing the signal against the overall background, the background must contain at least this many signals."),                
    make_option(c("-u", "--utr"), action="store_true", default=FALSE,
                help="If set, the signal must be in the 3'UTR of the gene. Not implemented."),
    make_option(c("-r", "--replicates"), type="integer", default=1,
                help="The signal must be found in at least this many replicates to be reported.")                
    )
opt = parse_args(OptionParser(option_list=option_list))

if(is.null(opt$gff)) {
    stop("You must specify a GFF file.")
}

if(!file.exists(opt$gff)) {
    stop("The specified GFF file was not found.")
}

if(is.null(opt$sPARTAoutput)) {
    stop("You must specify a sPARTA output directory.")
}

if(!file.exists(opt$sPARTAoutput)) {
    stop("The specified sPARTA output directory was not found.")
}

source("scripts/genomic_translate.R")

sPARTA_files = translate_sPARTA(opt$sPARTAoutput, opt$gff)
PAGe_files = get_condition(sPARTA_files$PAGe)

#all_genes = rtracklayer::import(opt$gff, format="gff3")
#PAGe = read.table(PAGe_file, sep="\t")
#colnames(PAGe) = c("gene", "pos", "count", "genomicChr", "genomicPos")

find_regulation_signature <- function(PAGe, threshold, cov_factor, min_n_signal, tolerance_window) {
    res = list(Pos=NA, PARE=NA)
    max_count = max(PAGe$count)
    max_index = which(PAGe$count==max_count)
        
    if(length(max_index) == 1) {
        max_position = PAGe$genomicPos[max_index]

        min_tolerance = max_position - tolerance_window
        max_tolerance = max_position + tolerance_window
        
        subset_indices = (PAGe$genomicPos < min_tolerance) | 
                         (PAGe$genomicPos > max_tolerance)
                         
        if(sum(subset_indices) >= min_n_signal) {
            PAGe_subset = PAGe[subset_indices,, drop=FALSE]
            second_max = max(PAGe_subset$count) 
            
            if(second_max * cov_factor < max_count) {
                res$Pos = max_position
                res$PARE = max_count
            }
        }
    }
    
    return(res)
}

PAGe_data = lapply(PAGe_files, read.table, sep="\t", col.names=c("gene", "pos", "count", "genomicChr", "genomicPos"))
unique_genes = unique(unlist(lapply(PAGe_data, "[[", "gene")))
res = data.frame(Gene=unique_genes)
for(PAGe in names(PAGe_data)) {
    # Define result columns and populate them with NAs.
    pos_col = paste0(PAGe, ".pos")
    res[[pos_col]] = NA
    PARE_col = paste0(PAGe, ".PARE")    
    res[[PARE_col]] = NA
    
    # Cut on the number of times we will run the function by pre-filtering.
    genes = PAGe_data[[PAGe]] %>% 
        group_by(gene) %>% 
        summarize(Max=max(count), N=n()) %>% 
        filter(Max >= opt$threshold, N > opt$minnsignal) %>%
        pull(gene)
    
    for(gene in genes) {
        PAGe_subset = PAGe_data[[PAGe]][PAGe_data[[PAGe]]$gene==gene,]
        res_row = res$Gene==gene
        reg_sig = find_regulation_signature(PAGe_subset, opt$threshold, opt$scalefactor, opt$minnsignal, opt$window)
        res[[pos_col]][res_row] = reg_sig$Pos
        res[[PARE_col]][res_row] = reg_sig$PARE
    }
}

pos_columns = grepl("pos", colnames(res))
not_na_res = res[!apply(is.na(res[,pos_columns]), 1, all),]
replicate_filter = apply(not_na_res[,pos_columns], 1, function(x) { max(table(x)) > opt$replicates })
final_res = not_na_res[replicate_filter,]

final_res$consensus = as.integer(apply(final_res[,pos_columns], 1, function(x) {names(sort(table(x), decreasing=TRUE)[1])}))

genome_fasta=FaFile(opt$fasta)
gff=rtracklayer::import(opt$gff)

final_res$chr = seqnames(gff)[match(final_res$Gene, gff$ID)]
final_res$start = final_res$consensus - 10
final_res$end = final_res$consensus + 10
final_res$strand = strand(gff)[match(final_res$Gene, gff$ID)]
target_gr = GRanges(data.frame(seqnames=final_res$chr, start=final_res$start, end=final_res$end, strand=final_res$strand))
target_seq = getSeq(genome_fasta, target_gr)
mir_seq = reverseComplement(target_seq)


final_res$target = as.character(target_seq)
final_res$mirna = as.character(mir_seq)

write.table(final_res, file=opt$output, sep="\t", col.names=TRUE, row.names=FALSE, quote=FALSE)