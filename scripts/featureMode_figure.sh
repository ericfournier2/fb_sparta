sPARTA_OUT=$1
gff=$2
gene=$3
output=$4

module load bioinformatics/R/3.4.1
module load bioinformatics/samtools/1.3.2

# Convert PAGe results.
for i in $sPARTA_OUT/PAGe/*_PAGe
do
    perl scripts/PAGE_long.pl < $i > $i.long
    Rscript scripts/genomic_translation_PAGe.R $sPARTA_OUT $gff
done

# Convert validated.
Rscript scripts/genomic_translation_validated.R $sPARTA_OUT $gff

Rscript scripts/figure.R --force --gff $gff --gene $gene -o $output \
                         -r D0:donnees/RNA-seq/D0.bam,D2:donnees/RNA-seq/D2.bam,D5:donnees/RNA-seq/D5.bam \
                         -s Merged:donnees/sRNA-seq/merged_alignments.bam  \
                         -p D0:$sPARTA_OUT/PAGe/D0_PAGe.long.genomic,D2:$sPARTA_OUT/PAGe/D2_PAGe.long.genomic,D5:$sPARTA_OUT/PAGe/D5_PAGe.long.genomic \
                         -v D0:$sPARTA_OUT/output/D0_validated.genomic.txt,D2:$sPARTA_OUT/output/D2_validated.genomic.txt,D5:$sPARTA_OUT/output/D5_validated.genomic.txt
#R --args --gff $gff --gene $gene -o $output \
#         -r D0:donnees/RNA-seq/D0.bam,D2:donnees/RNA-seq/D2.bam,D5:donnees/RNA-seq/D5.bam \
#         -s Merged:donnees/sRNA-seq/merged_alignments.bam  \
#         -p D0:$sPARTA_OUT/PAGe/D0_PAGe.long.genomic,D2:$sPARTA_OUT/PAGe/D2_PAGe.long.genomic,D5:$sPARTA_OUT/PAGe/D5_PAGe.long.genomic \
#         -v D0:$sPARTA_OUT/output/D0_validated.genomic.txt,D2:$sPARTA_OUT/output/D2_validated.genomic.txt,D5:$sPARTA_OUT/output/D5_validated.genomic.txt