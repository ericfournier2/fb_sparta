my $gene="";
while(my $line=<STDIN>) {
    if(substr($line, 0, 1) eq ">") {
        chomp($line);
        $gene=substr($line, 1);
    } else {
        my @data = split("\t", $line);
        my @pos = split(",", $data[1]);
        
        foreach(@pos) {
            print $gene . "\t" . $_ . "\t" . $data[0] . "\n";
        }
    }
}
