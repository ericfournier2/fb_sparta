suppressMessages(library(GenomicRanges))

# Get input/output file names from the command-line.
args = commandArgs(trailingOnly=TRUE)
sPARTA_output = args[1]
original_gff3 = args[2]

translate_PAGe = function(sPARTA_output, original_gff3) {
    all_genes = rtracklayer::import(original_gff3, format="gff3")
    all_specific = all_genes[all_genes$type != "gene" & all_genes$type != "mRNA"]
    all_specific_grl = split(all_specific, gsub("\\.(three_prime_UTR|five_prime_UTR|CDS)\\..*", "", all_specific$ID))
    
    # Load and process PAGe files.
    PAGe_files = Sys.glob(file.path(sPARTA_output, "PAGe", "*_PAGe.long"))
    for(PAGe_file in PAGe_files) {
        PAGe_data = read.table(PAGe_file, sep="\t", header=FALSE, stringsAsFactors=FALSE)
        
        all_ranges = GRanges(paste0(PAGe_data[,1], ":", PAGe_data[,2]))
        new_pos = GenomicFeatures::mapFromTranscripts(all_ranges, all_specific_grl)
    
        PAGe_data$genomicChr = as.vector(seqnames(new_pos))
        PAGe_data$genomicPos = start(new_pos)
        write.table(PAGe_data, sep="\t", file=paste0(PAGe_file, ".genomic"))
    }
}

translate_PAGe(sPARTA_output, original_gff3)
