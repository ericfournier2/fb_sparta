config=$1

module load bioinformatics/R/3.4.1
module load bioinformatics/samtools/1.3.2

Rscript scripts/figure.R --force -c $config
